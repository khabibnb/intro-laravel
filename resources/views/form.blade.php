<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form</title>
</head>
<body>
    <form action="/welcome" method="POST">
        @csrf
        {{-- csrf untuk post put delete --}}
        <fieldset>
        <legend>Register di S4nb3r c0d3</legend>
        <p>
            <label>Nama Depan</label>
            <input type="text" name="depan" placeholder="Nama Depan..." />
        </p>
        <p>
            <label>Nama Belakang</label>
            <input type="text" name="belakang" placeholder="Nama Belakang..." />
        </p>
        <p>
            <label>Password:</label>
            <input type="password" name="password" placeholder="password..." />
        </p>
        <p>
            <input type="submit" name="submit" value="Login" />
        </p>
        </fieldset>
    </form>
</body>
</html>
